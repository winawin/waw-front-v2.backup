module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/assets/scss/variables/_colors.scss";
          @import "@/assets/scss/variables/_typography.scss";
        `
      }
    }
  },
  configureWebpack: {
    output: {
      crossOriginLoading: 'anonymous'
    }
  }
}
