export default $axios => $resource => ({
  loginLinkedIn () {
    return $axios.get(`${$resource}/login/linkedin`)
  },
  getUserProfile (code, state) {
    return $axios.get(`${$resource}/login/linkedin/getProfile?code=${code}&state=${state}`)
  }
})
