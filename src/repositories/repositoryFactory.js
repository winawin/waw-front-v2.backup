import Vue from 'vue'
import createAuthRepository from './authRepository'
import createGeneralRepository from './generalRepository'
import createOffersRepository from './offersRepository'

const $axios = Vue.axios

// Auth
const authRepository = createAuthRepository($axios)
// Strapi
const generalRepository = createGeneralRepository($axios)
// Offers
const offersRepository = createOffersRepository($axios)

const apiUrl = process.env.NODE_ENV === 'development' ? 'https://winawin.net/v2/dev/api' : 'https://winawin.net/v2/api'
const strapiUrl = 'https://winawin.net/strapi'

const repositories = {
  auth: authRepository(apiUrl),
  general: generalRepository(strapiUrl),
  offers: offersRepository(apiUrl)
}

export default {
  get: name => repositories[name]
}
