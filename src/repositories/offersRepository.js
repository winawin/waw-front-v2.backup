export default $axios => $resource => ({
  getWizardOffers (payload) {
    return $axios.post(`${$resource}/offer`, payload)
  }
})
