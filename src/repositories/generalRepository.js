export default $axios => $resource => ({
  getHomepageData () {
    return $axios.get(`${$resource}/home`)
  },
  getPrivacyPolicy () {
    return $axios.get(`${$resource}/privacy-policy`)
  },
  getTermsOfService () {
    return $axios.get(`${$resource}/terms-of-service`)
  }
})
