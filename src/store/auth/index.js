import Repositories from '@/repositories/repositoryFactory'
import { Vue } from 'vue-property-decorator'
import router from '../../router'

const $authRepository = Repositories.get('auth')

const state = () => ({
  token: '',
  authState: '',
  userProfile: null,
  loading: false
})

const mutations = {
  setToken (state, token) {
    state.token = token
  },
  setAuthState (state, authState) {
    state.authState = authState
  },
  setUserProfile (state, userProfile) {
    state.userProfile = userProfile
  },
  setLoading (state, loading) {
    state.loading = loading
  }
}

const actions = {
  async loginLinkedIn ({ commit, state }) {
    if (!state.loading) {
      commit('setLoading', true)
      try {
        const { data } = await $authRepository.loginLinkedIn()
        const reqURL = process.env.NODE_ENV === 'development'
          ? data.reqURL.replace('https%3A%2F%2Fwww.winawin.net%2Fauth%2Flinkedin%2Fcallback', 'http://localhost:8080/auth/linkedin/callback')
          : data.reqURL
        window.location.href = reqURL
      } catch (e) {
        commit('setLoading', false)
        console.log(e)
      }
    }
  },
  async fetchUserProfile ({ commit, state }) {
    if (!state.loading) {
      commit('setLoading', true)
      try {
        const { data: userProfile } = await $authRepository.getUserProfile(state.token, state.authState)
        commit('setUserProfile', userProfile)
        commit('setLoading', false)
        return userProfile
      } catch (e) {
        commit('setLoading', false)
        console.log(e)
      }
    }
  },
  logout ({ commit }) {
    commit('setToken', '')
    commit('setAuthState', '')
    commit('setUserProfile', null)
    Vue.toasted.show('Successfully logged out!', { type: 'success' })
    router.push('/')
  }
}

const getters = {
  isAuthenticated (state) {
    return state.token !== '' && state.userProfile
  },
  isLoading (state) {
    return state.loading
  },
  getUserProfile (state) {
    return state.userProfile
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
