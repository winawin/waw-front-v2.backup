import Repositories from '@/repositories/repositoryFactory'
const $offersRepository = Repositories.get('offers')

const state = () => ({
  currentMatches: [],
  savedMatches: [],
  wizardData: {},
  loading: false,
  wizardComplete: false
})

const mutations = {
  SET_WIZARD_DATA (state, data) {
    state.wizardData = data
  },
  SET_LOADING (state, loading) {
    state.loading = loading
  },
  SET_WIZARD_COMPLETE (state, status) {
    state.wizardComplete = status
  },
  SET_MATCHES (state, matches) {
    state.currentMatches = matches
  }
}

const actions = {
  async fetchWizardOffers ({ commit, state }) {
    if (!state.loading) {
      commit('SET_LOADING', true)
      try {
        const { data: matches } = await $offersRepository.getWizardOffers(state.wizardData)
        console.log(matches)
        commit('SET_MATCHES', matches)
        commit('SET_LOADING', false)
        return matches
      } catch (e) {
        commit('SET_LOADING', false)
        console.log(e)
      }
    }
  }
}

const getters = {
  getCurrentMatches: state => state.currentMatches
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
