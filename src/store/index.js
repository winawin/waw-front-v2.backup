import Vue from 'vue'
import Vuex from 'vuex'
// Matches module
import matches from './matches'
// Auth module
import auth from './auth'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    matches,
    auth
  }
})
