import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
// Dynamic pages
const Home = () => import('../views/Home.vue')
const Wizard = () => import('../views/Wizard.vue')
const Tags = () => import('../views/Tags.vue')
const Matches = () => import('../views/Matches.vue')
const Partnerships = () => import('../views/Partnerships.vue')
const Offers = () => import('../views/Offers.vue')
const Wallet = () => import('../views/Wallet.vue')
const Callback = () => import('../views/Callback.vue')
const Profile = () => import('../views/Profile.vue')
const Settings = () => import('../views/Settings.vue')
const PrivacyPolicy = () => import('../views/PrivacyPolicy.vue')
const TermsOfService = () => import('../views/TermsOfService.vue')
// Use
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/wizard',
    name: 'Wizard',
    component: Wizard,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/tags',
    name: 'Tags',
    component: Tags,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/matches',
    name: 'Matches',
    component: Matches,
    meta: {
      layout: 'WLayoutDashboard',
      requiresAuth: true
    }
  },
  {
    path: '/partnerships',
    name: 'Partnerships',
    component: Partnerships,
    meta: {
      layout: 'WLayoutDashboard',
      requiresAuth: true
    }
  },
  {
    path: '/offers',
    name: 'Offers',
    component: Offers,
    meta: {
      layout: 'WLayoutDashboard',
      requiresAuth: true
    }
  },
  {
    path: '/wallet',
    name: 'Wallet',
    component: Wallet,
    meta: {
      layout: 'WLayoutDashboard',
      requiresAuth: true
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      layout: 'WLayoutDashboard',
      requiresAuth: true
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    meta: {
      layout: 'WLayoutDashboard',
      requiresAuth: true
    }
  },
  {
    path: '/auth/linkedin/callback',
    name: 'Callback',
    component: Callback
  },
  {
    path: '/terms-of-service',
    name: 'TermsOfService',
    component: TermsOfService
  },
  {
    path: '/privacy-policy',
    name: 'PrivacyPolicy',
    component: PrivacyPolicy
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach(async (to, from, next) => {
  const authenticatedUser = store.getters['auth/isAuthenticated']
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  // Check for protected route
  if (requiresAuth && !authenticatedUser) {
    sessionStorage.setItem('redirectPath', to.path)
    await store.dispatch('auth/loginLinkedIn')
  } else {
    next()
  }
})

export default router
