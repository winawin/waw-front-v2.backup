// Base
import Vue from 'vue'
import App from './App.vue'
// Plugins
import './plugins/vue-axios'
import './plugins/vue-toasted'
// Router
import router from './router'
// Vuex
import store from './store'
// Global Styles
import 'flexboxgrid2/flexboxgrid2.min.css'
import '@/assets/scss/base.scss'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
